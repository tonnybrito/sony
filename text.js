$(document).ready(function(){
  start_slider(5000);
});

// Começa o slider
function start_slider(tempo){
  // Configura os slides (as larguras, posicionamento etc)
  configure_slides();

  // Começa o contador que vai disparar a animação de slide a cada X milisegundos, definidos pela variável "tempo"
  setInterval(next_slide, tempo);
}

// Função que executa a animação que chama o próximo slide
function next_slide(){
  // Salva o número de slides na variável "num_slides"
  var num_slides = $('.slider .slide').length

  // Salva a largura do .slide-container na variável "width"
  var width = $('.slider-container').width();

  // Salva o valor que está no margin-left do elemento .slide na variável "margin_left"
  var margin_left = $('.slider').css('margin-left').replace('px', '') * -1;

  // Checa se a margin-left do .slider é maior que o "número de slides" vezes a largura de cada slide
  // Aqui o número de slides é subtraído de 1 pois como o primeiro slide já está aparecendo ele é desconsiderado para a conta dar certo
  // Se a margin-left do .slider é igual quer dizer que a animação já está exibindo o último slide, então a margin-left volta a ser ZERO
  if((num_slides-1) * width == margin_left) {
    $('.slider-container .slider').animate({'margin-left':'0'});
  }
  // Se a margin-left for diferent, então o .slider tem o margin-left é setado pra ser igual ao margin-left atual menos a largura de um slide.
  else {
    $('.slider-container .slider').animate({'margin-left':'-='+width});
  }
}

function configure_slides(){
  // Navega por todos os elementos que tenham a classe .slide
  $('.slider .slide').each(function(){
    
    // Salva o elemento atual na variável $this. Isso facilita o acesso ao elemento atual mais adiante no código
    var $this = $(this);

    // Salva o elemento que vai deslizar para os lados na variável $slider
    var $slider = $this.parent();

    // Salva o elemento que vai conter tudo na variável $slider_container
    var $slider_container = $slider.parent();

    // Salva a imagem do slide atual na variável $img
    var $img = $this.find('img');

    // Pega a largura e altura do .slider-container
    var slider_width = $slider_container.width();
    var slider_height = $slider_container.height();

    // Seta a largura do slide atual igual a largura do $slider_container
    $this.width(slider_width);

    // Calcula o valor do TOP que vai ser setado para a imagem pra ela ficar centralizada verticalmente
    var img_top = 0
    if($img.height() > slider_height){
      img_top = (slider_height - $img.height()) / 2;
    }
    $img.css('top', img_top);
  });
}